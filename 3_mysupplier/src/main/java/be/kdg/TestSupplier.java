package be.kdg;

import be.kdg.supplier.MySupplier;

public class TestSupplier {
    public static void main(String[] args) {
        MySupplier supplier = new MySupplier();

        System.out.println(supplier.returnPiloot());
        // Vervang de puntjes door de juiste lambda expression
        // zodat je de verwachte afdruk bekomt.
        //
        // System.out.println(supplier.groet(...);

    }
}

/*
Verstappen 33 Torro Rosso
Hello Max!
 */